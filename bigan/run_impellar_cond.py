
import time
import re
import numpy as np
import tensorflow as tf
import pickle
import logging
import importlib
import random
import sys
import os
import bigan.mnist_utilities_cond as network
import data.mnist as data
from scipy.ndimage import gaussian_filter
from sklearn.preprocessing import OneHotEncoder
from utils.evaluations import do_prc
import scipy.misc as ski

RANDOM_SEED = 13
FREQ_PRINT = 20  # print frequency image tensorboard [20]
n_labels = 121
thresh = .01
n_segs = int(np.sqrt(n_labels))
input_sh = data.get_shape_input() # (None, 28, 28, 1)
label_sh = (None, n_labels)
src = 'bigan/DS2_3289_BW_original_June_28x28/train/'
nd_dir = src + '0/'
d_dir = src + '1/'

def get_getter(ema):  # to update neural net with moving avg variables, suitable for ss learning cf Saliman
    def ema_getter(getter, name, *args, **kwargs):
        var = getter(name, *args, **kwargs)
        ema_var = ema.average(var)
        return ema_var if ema_var else var

    return ema_getter

def make_soft_labels():
    encv = OneHotEncoder(n_values=n_labels, handle_unknown='ignore')
    lbls = np.arange(n_labels)
    lbls = encv.fit_transform(lbls.reshape(list(lbls.shape) + [-1])).toarray()
    print('lbls shape: {}'.format(lbls.shape))
    time.sleep(10)
    lbls = lbls.reshape((n_labels, n_labels))
    p_lbls = np.zeros((n_labels, n_labels))
    for k, lbl in enumerate(lbls):
        p_lbl = lbl.reshape((n_segs, n_segs))
        p_lbl = gaussian_filter(p_lbl, sigma=.5)
        p_lbl[p_lbl < thresh] = 0
        p_lbls[k] = (p_lbl / p_lbl.sum()).ravel()
    return p_lbls

def load_batch(fnames):
    bX = np.zeros((len(fnames), input_sh[1], input_sh[2], input_sh[3]))
    for i, fname in enumerate(fnames):
        im = ski.imread(fname, mode='L').astype(float)
        bX[i] = (im/127.5 - 1).reshape(28, 28, 1) # Input scaled to [-1, 1]
    return bX

def display_parameters(batch_size, starting_lr, ema_decay,
                       weight, method, degree, label):
    '''See parameters
    '''
    print('Batch size: ', batch_size)
    print('Starting learning rate: ', starting_lr)
    print('EMA Decay: ', ema_decay)
    print('Weight: ', weight)
    print('Method for discriminator: ', method)
    print('Degree for L norms: ', degree)
    print('Anomalous label: ', label)


def display_progression_epoch(j, id_max):
    '''See epoch progression
    '''
    batch_progression = int((j / id_max) * 100)
    sys.stdout.write(str(batch_progression) + ' % epoch' + chr(13))
    _ = sys.stdout.flush


def create_logdir(method, weight, label, rd):
    """ Directory to save training logs, weights, biases, etc."""
    return "bigan_cond/train_logs/impellar/{}/{}/{}/{}".format(weight, method, label, rd)


def train_and_test(nb_epochs, weight, method, degree, random_seed, label):
    """ Runs the Bigan on the MNIST dataset

    Note:
        Saves summaries on tensorboard. To display them, please use cmd line
        tensorboard --logdir=model.training_logdir() --port=number
    Args:
        nb_epochs (int): number of epochs
        weight (float, optional): weight for the anomaly score composition
        method (str, optional): 'fm' for ``Feature Matching`` or "cross-e"
                                     for ``cross entropy``, "efm" etc.
        anomalous_label (int): int in range 0 to 10, is the class/digit
                                which is considered outlier
    """
    # logger = logging.getLogger("BiGAN.train.mnist.{}.{}".format(method, label))
    logger = logging.getLogger("BiGAN.train.impellar.{}.{}".format(method, label))

    # Placeholders
    input_pl = tf.placeholder(tf.float32, shape=input_sh, name="input")
    label_pl = tf.placeholder(tf.float32, shape=label_sh, name="label")
    is_training_pl = tf.placeholder(tf.bool, [], name='is_training_pl')
    learning_rate = tf.placeholder(tf.float32, shape=(), name="lr_pl")

    soft_labels = make_soft_labels()

    # Data
    suffix = '\''
    ndx = os.listdir(nd_dir)
    n_ndx = []
    n_ndy = []
    for k, fn in enumerate(ndx):
        if fn.endswith(suffix):
            continue
        else:
            n_ndx.append(nd_dir + fn)
            n_ndy.append(soft_labels[int(re.findall(r'\d+', fn)[-1])])
    ndx = n_ndx
    ndy = n_ndy

    ndxy = list(zip(ndx, ndy))
    random.shuffle(ndxy)
    ndx, ndy = list(zip(*ndxy))
    ndx, ndy = list(ndx), list(ndy)

    p_T = .9
    print(ndx[:10])
    trainx = ndx[:int(p_T * len(ndx))]
    trainx_copy = trainx.copy()

    trainy = np.array(ndy[:int(p_T * len(ndx))])
    trainy_copy = trainy.copy()
    print(trainx[:10])
    print(trainy[:10])
    print('-' * 30)
    # time.sleep(10)

    testndx = ndx[int(p_T * len(ndx)):]
    testndy = np.array(ndy[int(p_T * len(ndx)):])
    print(testndx[:10])
    print(testndy[:10])
    print('-' * 30)
    # time.sleep(10)

    testdx = os.listdir(d_dir)
    n_testdx = []
    n_testdy = []
    for k, fn in enumerate(testdx):
        if fn.endswith(suffix):
            continue
        else:
            n_testdx.append(d_dir + fn)
            n_testdy.append(soft_labels[int(re.findall(r'\d+', fn)[-1])])
    testdx = n_testdx
    testdy = np.array(n_testdy)
    print(testdx[:10])
    print(testdy[:10])
    print('-'*30)
    # time.sleep(10)

    testx = testndx + testdx
    testadvy = np.concatenate((np.zeros(len(testndx)), np.ones(len(testdx))))
    testsupy = np.concatenate((testndy, testdy))

    print('trainx shape: {}'.format(len(trainx)))
    print('trainy shape: {}'.format(trainy.shape))
    print('testx shape: {}'.format(len(testx)))
    print('testadvy shape: {}'.format(len(testadvy)))
    print('testsupy shape: {}'.format(len(testsupy)))
    # time.sleep(10)

    # Parameters
    starting_lr = network.learning_rate
    batch_size = network.batch_size
    latent_dim = network.latent_dim
    ema_decay = 0.999

    nr_batches_train = int(len(trainx) / batch_size)
    nr_batches_test = int(len(testx) / batch_size)

    logger.info('Building training graph...')

    logger.warn("The BiGAN is training with the following parameters:")
    display_parameters(batch_size, starting_lr, ema_decay, weight, method, degree, label)

    gen = network.decoder
    enc = network.encoder
    dis = network.discriminator

    with tf.variable_scope('encoder_model'):
        z_gen = enc(input_pl, label_pl, is_training=is_training_pl)

    with tf.variable_scope('generator_model'):
        z = tf.random_normal([batch_size, latent_dim])
        # rlabels = tf.convert_to_tensor(soft_labels[np.random.randint(0, n_labels, batch_size)])
        rlabels = soft_labels[np.random.randint(0, n_labels, batch_size)]
        x_gen = gen(z, rlabels, is_training=is_training_pl)
        reconstruct = gen(z_gen, label_pl, is_training=is_training_pl, reuse=True)

    with tf.variable_scope('discriminator_model'):
        l_encoder, l_encoder_sup, inter_layer_inp = dis(z_gen, input_pl, label_pl, is_training=is_training_pl)
        l_generator, l_generator_sup, inter_layer_rct = dis(z, x_gen, rlabels, is_training=is_training_pl, reuse=True)

    with tf.name_scope('loss_functions'):
        l_encoder = tf.cast(l_encoder, tf.float32)
        l_generator = tf.cast(l_generator, tf.float32)
        rlabels = tf.cast(rlabels, tf.float32)
        l_encoder_sup = tf.cast(l_encoder_sup, tf.float32)
        l_generator_sup = tf.cast(l_generator_sup, tf.float32)

        # discriminator
        loss_dis_enc_adv = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=tf.ones_like(l_encoder), logits=l_encoder))
        loss_dis_enc_sup = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=label_pl, logits=l_encoder_sup))
        loss_dis_gen_adv = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=tf.zeros_like(l_generator), logits=l_generator))
        loss_dis_gen_sup = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=rlabels, logits=l_generator_sup))
        loss_discriminator = loss_dis_enc_adv + loss_dis_enc_sup + loss_dis_gen_adv  + loss_dis_gen_sup

        # encoder
        loss_enc_adv = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=tf.zeros_like(l_encoder), logits=l_encoder))
        loss_enc_sup = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=label_pl, logits=l_encoder_sup))
        loss_encoder = loss_enc_adv + loss_enc_sup

        # generator
        loss_gen_adv = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=tf.ones_like(l_generator), logits=l_generator))
        loss_gen_sup = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=rlabels, logits=l_generator_sup))
        loss_generator = loss_gen_adv + loss_gen_sup

    with tf.name_scope('optimizers'):
        # control op dependencies for batch norm and trainable variables
        tvars = tf.trainable_variables()
        dvars = [var for var in tvars if 'discriminator_model' in var.name]
        gvars = [var for var in tvars if 'generator_model' in var.name]
        evars = [var for var in tvars if 'encoder_model' in var.name]

        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        update_ops_gen = [x for x in update_ops if ('generator_model' in x.name)]
        update_ops_enc = [x for x in update_ops if ('encoder_model' in x.name)]
        update_ops_dis = [x for x in update_ops if ('discriminator_model' in x.name)]

        optimizer_dis = tf.train.AdamOptimizer(learning_rate=learning_rate, beta1=0.5, name='dis_optimizer')
        optimizer_gen = tf.train.AdamOptimizer(learning_rate=learning_rate, beta1=0.5, name='gen_optimizer')
        optimizer_enc = tf.train.AdamOptimizer(learning_rate=learning_rate, beta1=0.5, name='enc_optimizer')

        with tf.control_dependencies(update_ops_gen):
            gen_op = optimizer_gen.minimize(loss_generator, var_list=gvars)
        with tf.control_dependencies(update_ops_enc):
            enc_op = optimizer_enc.minimize(loss_encoder, var_list=evars)
        with tf.control_dependencies(update_ops_dis):
            dis_op = optimizer_dis.minimize(loss_discriminator, var_list=dvars)

        # Exponential Moving Average for estimation
        dis_ema = tf.train.ExponentialMovingAverage(decay=ema_decay)
        maintain_averages_op_dis = dis_ema.apply(dvars)

        with tf.control_dependencies([dis_op]):
            train_dis_op = tf.group(maintain_averages_op_dis)

        gen_ema = tf.train.ExponentialMovingAverage(decay=ema_decay)
        maintain_averages_op_gen = gen_ema.apply(gvars)

        with tf.control_dependencies([gen_op]):
            train_gen_op = tf.group(maintain_averages_op_gen)

        enc_ema = tf.train.ExponentialMovingAverage(decay=ema_decay)
        maintain_averages_op_enc = enc_ema.apply(evars)

        with tf.control_dependencies([enc_op]):
            train_enc_op = tf.group(maintain_averages_op_enc)

    with tf.name_scope('summary'):
        with tf.name_scope('dis_summary'):
            tf.summary.scalar('loss_discriminator', loss_discriminator, ['dis'])
            tf.summary.scalar('loss_dis_encoder_adversarial', loss_dis_enc_adv, ['dis'])
            tf.summary.scalar('loss_dis_encoder_supervised', loss_dis_enc_sup, ['dis'])
            tf.summary.scalar('loss_dis_generator_adversarial', loss_dis_gen_adv, ['dis'])
            tf.summary.scalar('loss_dis_generator_supervised', loss_dis_gen_sup, ['dis'])

        with tf.name_scope('gen_summary'):
            tf.summary.scalar('loss_generator', loss_generator, ['gen'])
            tf.summary.scalar('loss_encoder', loss_encoder, ['gen'])

        with tf.name_scope('image_summary'):
            tf.summary.image('reconstruct', reconstruct, 8, ['image'])
            tf.summary.image('input_images', input_pl, 8, ['image'])

        sum_op_dis = tf.summary.merge_all('dis')
        sum_op_gen = tf.summary.merge_all('gen')
        sum_op_im = tf.summary.merge_all('image')

    logger.info('Building testing graph...')

    with tf.variable_scope('encoder_model'):
        z_gen_ema = enc(input_pl, label_pl, is_training=is_training_pl, getter=get_getter(enc_ema), reuse=True)

    with tf.variable_scope('generator_model'):
        reconstruct_ema = gen(z_gen_ema, label_pl, is_training=is_training_pl, getter=get_getter(gen_ema), reuse=True)

    with tf.variable_scope('discriminator_model'):
        l_encoder_ema, l_encoder_sup, inter_layer_inp_ema = dis(z_gen_ema,
                                                                input_pl,
                                                                label_pl,
                                                                is_training=is_training_pl,
                                                                getter=get_getter(dis_ema),
                                                                reuse=True)
        l_generator_ema, l_generator_sup, inter_layer_rct_ema = dis(z_gen_ema,
                                                                    reconstruct_ema,
                                                                    label_pl,
                                                                    is_training=is_training_pl,
                                                                    getter=get_getter(dis_ema),
                                                                    reuse=True)

    with tf.name_scope('Testing'):
        with tf.variable_scope('Reconstruction_loss'):
            delta = input_pl - reconstruct_ema
            delta_flat = tf.contrib.layers.flatten(delta)
            gen_score = tf.norm(delta_flat, ord=degree, axis=1,
                                keep_dims=False, name='epsilon')

        with tf.variable_scope('Discriminator_loss'):
            if method == "cross-e":
                dis_score = tf.nn.sigmoid_cross_entropy_with_logits(
                    labels=tf.ones_like(l_generator_ema), logits=l_generator_ema)

            elif method == "fm":
                fm = inter_layer_inp_ema - inter_layer_rct_ema
                fm = tf.contrib.layers.flatten(fm)
                dis_score = tf.norm(fm, ord=degree, axis=1,
                                    keep_dims=False, name='d_loss')

            dis_score = tf.squeeze(dis_score)

        with tf.variable_scope('Score'):
            list_scores = (1 - weight) * gen_score + weight * dis_score

    logdir = create_logdir(weight, method, label, random_seed)

    sv = tf.train.Supervisor(logdir=logdir, save_summaries_secs=None,
                             save_model_secs=120)

    logger.info('Start training...')
    with sv.managed_session() as sess:

        logger.info('Initialization done')
        writer = tf.summary.FileWriter(logdir, sess.graph)
        train_batch = 0
        epoch = 0

        while not sv.should_stop() and epoch < nb_epochs:

            lr = starting_lr
            begin = time.time()

            # construct randomly permuted minibatches
            perm = np.random.permutation(len(trainx))
            r_trainx = []
            for k in perm:
                r_trainx.append(trainx[k])
            trainx = r_trainx
            trainy = trainy[perm]

            perm = np.random.permutation(len(trainx_copy))
            r_trainx_copy = []
            for k in perm:
                r_trainx_copy.append(trainx_copy[k])
            trainx_copy = r_trainx_copy
            trainy_copy = trainy_copy[perm]
            train_loss_dis, train_loss_gen, train_loss_enc = [0, 0, 0]

            # training
            for t in range(nr_batches_train):

                display_progression_epoch(t, nr_batches_train)
                ran_from = t * batch_size
                ran_to = (t + 1) * batch_size

                # train discriminator
                batchx = load_batch(trainx[ran_from:ran_to])
                batchy = trainy[ran_from:ran_to]

                batchx_copy = load_batch(trainx_copy[ran_from:ran_to])
                batchy_copy = trainy_copy[ran_from:ran_to]

                feed_dict = {input_pl: batchx,
                             label_pl: batchy,
                             is_training_pl: True,
                             learning_rate: lr}

                _, ld, sm = sess.run([train_dis_op,
                                      loss_discriminator,
                                      sum_op_dis],
                                     feed_dict=feed_dict)
                train_loss_dis += ld
                writer.add_summary(sm, train_batch)

                # train generator and encoder
                feed_dict = {input_pl: batchx_copy,
                             label_pl: batchy_copy,
                             is_training_pl: True,
                             learning_rate: lr}
                _, _, le, lg, sm = sess.run([train_gen_op,
                                             train_enc_op,
                                             loss_encoder,
                                             loss_generator,
                                             sum_op_gen],
                                            feed_dict=feed_dict)
                train_loss_gen += lg
                train_loss_enc += le
                writer.add_summary(sm, train_batch)

                if t % FREQ_PRINT == 0:  # inspect reconstruction
                    t = np.random.randint(0, 4000)
                    ran_from = t
                    ran_to = t + batch_size
                    rbatchx = load_batch(trainx[ran_from:ran_to])
                    rbatchy = trainy[ran_from:ran_to]
                    sm = sess.run(sum_op_im, feed_dict={input_pl: rbatchx, label_pl: rbatchy, is_training_pl: False})
                    writer.add_summary(sm, train_batch)

                train_batch += 1

            train_loss_gen /= nr_batches_train
            train_loss_enc /= nr_batches_train
            train_loss_dis /= nr_batches_train

            logger.info('Epoch terminated')
            print("Epoch %d | time = %ds | loss gen = %.4f | loss enc = %.4f | loss dis = %.4f "
                  % (epoch, time.time() - begin, train_loss_gen, train_loss_enc, train_loss_dis))

            epoch += 1

        logger.warn('Testing evaluation...')

        pickle.dump(testx, open(logdir + 'filename.pickle', 'wb'),
                    protocol=pickle.HIGHEST_PROTOCOL)
        np.save(logdir + 'testadvy.npy', np.array(testadvy))
        np.save(logdir + 'testsupy.npy', np.array(testsupy))

        scores = []
        inference_time = []

        # Create scores
        for t in range(nr_batches_test):
            # construct randomly permuted minibatches
            ran_from = t * batch_size
            ran_to = (t + 1) * batch_size
            begin_val_batch = time.time()

            batcht = load_batch(testx[ran_from:ran_to])
            batchy = testsupy[ran_from:ran_to]

            feed_dict = {input_pl: batcht,
                         label_pl: batchy,
                         is_training_pl: False}

            scores += sess.run(list_scores,
                               feed_dict=feed_dict).tolist()

            inference_time.append(time.time() - begin_val_batch)

        logger.info('Testing : mean inference time is %.4f' % (
            np.mean(inference_time)))

        size = len(testx)

        fillx = np.ones([size - ran_to, 28, 28, 1])
        batcht = load_batch(testx[ran_to:])
        batcht = np.concatenate([batcht, fillx], axis=0)

        filly = np.zeros((size - ran_to, n_labels))
        batchy = testsupy[ran_to:]
        batchy = np.concatenate([batchy, filly], axis=0)

        feed_dict = {input_pl: batcht,
                     label_pl: batchy,
                     is_training_pl: False}

        batch_score = sess.run(list_scores,
                               feed_dict=feed_dict).tolist()

        scores += batch_score[:size]

        testx, testadvy, testsupy, scores = testx[:size], testadvy[:size], testsupy[:size], scores[:size]

        np.save(logdir + 'scores.npy', np.array(scores))

        prc_auc = do_prc(scores, testadvy,
                         file_name=r'bigan/impellar/{}/{}/{}'.format(method, weight, label),
                         directory=r'results/bigan/impellar/{}/{}/'.format(method, weight))

        print("Testing | PRC AUC = {:.4f}".format(prc_auc))

def run(nb_epochs, weight, method, degree, label, random_seed=42):
    """ Runs the training process"""
    with tf.Graph().as_default():
        # Set the graph level seed
        tf.set_random_seed(random_seed)
        train_and_test(nb_epochs, weight, method, degree, random_seed, label)
