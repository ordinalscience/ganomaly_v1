
import tensorflow as tf
import time

"""
MNIST conditional BiGAN architecture.

Use soft labelling for classification.

Generator (decoder), encoder and discriminator.

"""

learning_rate = 0.00001
batch_size = 100
layer = 1
latent_dim = 200
dis_inter_layer_dim = 1024
init_kernel = tf.random_normal_initializer(mean=0.0, stddev=0.02)
n_labels = 121


import tensorflow as tf
from tensorflow.python.framework import ops
#
# if "concat_v2" in dir(tf):
#   def concat(tensors, axis, *args, **kwargs):
#     return tf.concat_v2(tensors, axis, *args, **kwargs)
# else:
#   def concat(tensors, axis, *args, **kwargs):
#     return tf.concat(tensors, axis, *args, **kwargs)

def conv_cond_concat(x, y):
  """Concatenate conditioning vector on feature map axis."""
  x_shapes = x.get_shape()
  y_shapes = y.get_shape()
  # return tf.concat([x, y*tf.ones([y_shapes[0], x_shapes[1], x_shapes[2], y_shapes[3]])], axis=3)
  return tf.concat([x, y*tf.ones([batch_size, x_shapes[1], x_shapes[2], y_shapes[3]])], axis=3)

def encoder(x_inp, label, is_training=False, getter=None, reuse=False):
    """ Encoder architecture in tensorflow

    Maps the data into the latent space

    Args:
        x_inp (tensor): input data for the encoder.
        reuse (bool): sharing variables or not

    Returns:
        (tensor): last activation layer of the encoder

    """
    with tf.variable_scope('encoder', reuse=reuse, custom_getter=getter):

        x_inp = tf.reshape(x_inp, [-1, 28, 28, 1])
        yb = tf.reshape(label, [-1, 1, 1, n_labels])
        h0 = conv_cond_concat(x_inp, yb)

        name_net = 'layer_1'
        with tf.variable_scope(name_net):
            h1 = tf.layers.conv2d(h0,
                                   32,
                                   [3, 3],
                                   padding='SAME',
                                   kernel_initializer=init_kernel,
                                   name='conv')
            h1 = leakyReLu(h1, name='leaky_relu')

        h1 = conv_cond_concat(h1, yb)

        name_net = 'layer_2'
        with tf.variable_scope(name_net):
            h2 = tf.layers.conv2d(h1,
                                   64,
                                   [3, 3],
                                   padding='SAME',
                                   strides=[2, 2],
                                   kernel_initializer=init_kernel,
                                   name='conv')
            h2 = tf.layers.batch_normalization(h2, training=is_training)
            h2 = leakyReLu(h2, name='leaky_relu')

        h2 = conv_cond_concat(h2, yb)

        name_net = 'layer_3'
        with tf.variable_scope(name_net):
            h3 = tf.layers.conv2d(h2,
                                   128,
                                   [3, 3],
                                   padding='SAME',
                                   strides=[2, 2],
                                   kernel_initializer=init_kernel,
                                   name='conv')
            h3 = tf.layers.batch_normalization(h3, training=is_training)
            h3 = leakyReLu(h3, name='leaky_relu')

        h3 = conv_cond_concat(h3, yb)

        h3 = tf.contrib.layers.flatten(h3)

        name_net = 'layer_4'
        with tf.variable_scope(name_net):
            h4 = tf.layers.dense(h3,
                                  units=latent_dim,
                                  kernel_initializer=init_kernel,
                                  name='fc')

    return h4

def decoder(z_inp, label, is_training=False, getter=None, reuse=False):
    """ Decoder architecture in tensorflow

    Generates data from the latent space

    Args:
        z_inp (tensor): variable in the latent space
        reuse (bool): sharing variables or not

    Returns:
        (tensor): last activation layer of the generator

    """
    with tf.variable_scope('generator', reuse=reuse, custom_getter=getter):

        yb = tf.cast(tf.reshape(label, [-1, 1, 1, n_labels]), tf.float32)
        h0 = tf.concat([z_inp, tf.cast(label, tf.float32)], 1)

        name_net = 'layer_1'
        with tf.variable_scope(name_net):

            h1 = tf.layers.dense(h0,
                                  units=1024,
                                  kernel_initializer=init_kernel,
                                  name='fc')
            h1 = tf.layers.batch_normalization(h1,
                                        training=is_training,
                                        name='batch_normalization')
            h1 = tf.nn.relu(h1, name='relu')

        h1 = tf.concat([h1, tf.cast(label, tf.float32)], 1)

        name_net = 'layer_2'
        with tf.variable_scope(name_net):
            h2 = tf.layers.dense(h1,
                                  units=7*7*128,
                                  kernel_initializer=init_kernel,
                                  name='fc')
            h2 = tf.layers.batch_normalization(h2,
                                        training=is_training,
                                        name='batch_normalization')
            h2 = tf.nn.relu(h2, name='relu')

        h2 = tf.reshape(h2, [-1, 7, 7, 128])

        h2 = conv_cond_concat(h2, yb)

        name_net = 'layer_3'
        with tf.variable_scope(name_net):
            h3 = tf.layers.conv2d_transpose(h2,
                                     filters=64,
                                     kernel_size=4,
                                     strides= 2,
                                     padding='same',
                                     kernel_initializer=init_kernel,
                                     name='conv')
            h3 = tf.layers.batch_normalization(h3,
                                        training=is_training,
                                        name='batch_normalization')
            h3 = tf.nn.relu(h3, name='relu')

        h3 = conv_cond_concat(h3, yb)

        name_net = 'layer_4'
        with tf.variable_scope(name_net):
            h4 = tf.layers.conv2d_transpose(h3,
                                     filters=1,
                                     kernel_size=4,
                                     strides=2,
                                     padding='same',
                                     kernel_initializer=init_kernel,
                                     name='conv')
            h4 = tf.tanh(h4, name='tanh')

    return h4

def discriminator(z_inp, x_inp, label, is_training=False, getter=None, reuse=False):
    """ Discriminator architecture in tensorflow

    Discriminates between pairs (E(x), x) and (z, G(z))

    Args:
        z_inp (tensor): variable in the latent space
        x_inp (tensor): input data for the encoder.
        reuse (bool): sharing variables or not

    Returns:
        logits (tensor): last activation layer of the discriminator
        intermediate_layer (tensor): intermediate layer for feature matching

    """
    with tf.variable_scope('discriminator', reuse=reuse, custom_getter=getter):

        # D(x)
        x_inp = tf.reshape(x_inp, [-1, 28, 28, 1])
        yb = tf.cast(tf.reshape(label, [-1, 1, 1, n_labels]), tf.float32)
        h0 = conv_cond_concat(x_inp, yb)

        name_net = 'x_layer_1'
        with tf.variable_scope(name_net):
            h1 = tf.layers.conv2d(h0,
                           filters=64,
                           kernel_size=4,
                           strides=2,
                           padding='same',
                           kernel_initializer=init_kernel,
                           name='conv')
            h1 = leakyReLu(h1, 0.1, name='leaky_relu')
            h1 = tf.layers.dropout(h1, rate=0.5, name='dropout',
                                  training=is_training)

        h1 = conv_cond_concat(h1, yb)

        name_net = 'x_layer_2'
        with tf.variable_scope(name_net):
            h2 = tf.layers.conv2d(h1,
                           filters=64,
                           kernel_size=4,
                           strides=2,
                           padding='same',
                           kernel_initializer=init_kernel,
                           name='conv')
            h2 = tf.layers.batch_normalization(h2,
                                        training=is_training,
                                        name='batch_normalization')
            h2 = leakyReLu(h2, 0.1, name='leaky_relu')
            h2 = tf.layers.dropout(h2, rate=0.5, name='dropout',
                                  training=is_training)

        x = tf.reshape(h2, [-1, 7 * 7 * 64])

        # D(z)
        name_z = 'z_layer_1'
        with tf.variable_scope(name_z):
            z = tf.layers.dense(z_inp,
                                512,
                                kernel_initializer=init_kernel,
                                name='fc')
            z = leakyReLu(z, name='leaky_relu')
            z = tf.layers.dropout(z, rate=0.5, name='dropout',
                                  training=is_training)

        ##############################
        # D(x,z)
        # For adversarial loss
        adv0 = tf.concat([x, z], axis=1)

        name_adv = 'adv_layer_1'
        with tf.variable_scope(name_adv):
            adv1 = tf.layers.dense(adv0,
                                dis_inter_layer_dim,
                                kernel_initializer=init_kernel,
                                name='fc')
            adv1 = leakyReLu(adv1, name='leaky_relu')
            adv1 = tf.layers.dropout(adv1, rate=0.5, name='dropout', training=is_training)

        intermediate_layer = adv1
        name_adv = 'adv_fc_logits'
        with tf.variable_scope(name_adv):
            adv_logits = tf.layers.dense(adv1, 1, kernel_initializer=init_kernel, name='fc')

        ##############################
        # D(x)
        # For supervised loss
        name_sup = 'sup_layer_1'
        with tf.variable_scope(name_sup):
            sup1 = tf.layers.dense(x,
                                dis_inter_layer_dim,
                                kernel_initializer=init_kernel,
                                name='fc')
            sup1 = leakyReLu(sup1, name='leaky_relu')
            sup1 = tf.layers.dropout(sup1, rate=0.5, name='dropout', training=is_training)

        name_sup = 'sup_fc_logits'
        with tf.variable_scope(name_sup):
            sup_logits = tf.layers.dense(sup1, n_labels, kernel_initializer=init_kernel, name='fc')

        ##############################

    return adv_logits, sup_logits, intermediate_layer

def leakyReLu(x, alpha=0.1, name=None):
    if name:
        with tf.variable_scope(name):
            return _leakyReLu_impl(x, alpha)
    else:
        return _leakyReLu_impl(x, alpha)

def _leakyReLu_impl(x, alpha):
    return tf.nn.relu(x) - (alpha * tf.nn.relu(-x))

