
import numpy as np
import tensorflow as tf
import os
import pickle
import bigan.mnist_utilities as network
from torchvision import transforms
import scipy.misc as ski
from fextractor_2_w_config import FeatureExtractor_2

# SETABLE PARAMS
##########################################

datadir = '/home/dev/data/xpo-cv/'
ldir = '/home/dev/ganomaly_v1/bigan/'
model_dir = '/Users/evankingstad/bitbucket/ganomaly_v1/bigan/train_logs/impellar/fm/0.1/1/42/'
degree = 1
weight = .1
n_ch = 1
aug_factor = 10
slc_dim = 28
n_slcs = 121
if n_ch == 1:
    mode = 'L'
elif n_ch == 3:
    mode = 'RGB'
feat_dim = network.dis_inter_layer_dim # 1024
batch_size = network.batch_size
config_dc = pickle.load(open('{}config_dc.p'.format(datadir), 'rb'))

# METHODS
##########################################

#
def coord_to_ind(i, j, n_segs):
    ind = int(i * n_segs + j)
    return ind

#
def get_img_slices(arr, slice_dim):
    arr = np.squeeze(arr)
    shape_ = np.shape(arr)[:2]
    assert shape_[0] == shape_[1]
    arr_dim = shape_[0]
    slice_dim = min(shape_[0], shape_[1], slice_dim)
    n_segs = arr_dim//slice_dim
    if len(arr.shape) > 2:
        slices = np.empty((n_segs ** 2, slice_dim, slice_dim, arr.shape[-1])).astype(arr.dtype)
    else:
        slices = np.empty((n_segs**2, slice_dim, slice_dim)).astype(arr.dtype)
    for i in range(n_segs):
        y_init = i*slice_dim
        y_fin = (i + 1)*slice_dim
        for j in range(n_segs):
            x_init = j * slice_dim
            x_fin = (j + 1) * slice_dim
            ind = coord_to_ind(i, j, n_segs)
            slices[ind] = arr[y_init:y_fin, x_init:x_fin]
    return slices

#
def make_predictions(b_img, slc_dim):

    global input_pl
    global is_training_pl
    global gen_score
    global logitsExDx
    global featsExDx
    global logitsExDGEx
    global featsExDGEx
    global n

    assert b_img.shape[0] == b_img.shape[1]
    if b_img.shape[0] == b_img.shape[1] > slc_dim:
        sl_x = get_img_slices(b_img, slc_dim)
    else:
        sl_x = b_img

    if sl_x.dtype != np.uint8:
        sl_x = sl_x.astype(np.uint8)

    if len(sl_x.shape) == 3:
        n = 1
        sl_x = np.expand_dims(sl_x, axis=0)
    else:
        n = sl_x.shape[0]

    imgen_scores = np.zeros(n)
    imlogitsExDx = np.zeros(n)
    imfeatsExDx = np.zeros((n, feat_dim))
    imlogitsExDGEx = np.zeros(n)
    imfeatsExDGEx = np.zeros((n, feat_dim))

    rem = n%batch_size
    n_batches = n//batch_size + int(rem>0)
    for b in np.arange(1, n_batches+1):

        s_ind, e_ind = b*batch_size, min((b+1)*batch_size, n)
        batchx = sl_x[s_ind:e_ind]
        if b < n_batches:
            feed_dict = {input_pl: batchx, is_training_pl: False}
        else:
            fill = np.ones([batch_size-rem, slc_dim, slc_dim, n_ch])
            batchx = np.concatenate([batchx, fill], axis=0)
            feed_dict = {input_pl: batchx, is_training_pl: False}

        bgen_scores, blogitsExDx, bfeatsExDx, blogitsExDGEx, bfeatsExDGEx = \
            sess.run([gen_score, logitsExDx, featsExDx, logitsExDGEx, featsExDGEx], feed_dict=feed_dict)

        bgen_scores = bgen_scores[:e_ind-s_ind]
        blogitsExDx = blogitsExDx[:e_ind-s_ind]
        bfeatsExDx = bfeatsExDx[:e_ind-s_ind]
        blogitsExDGEx = blogitsExDGEx[:e_ind-s_ind]
        bfeatsExDGEx = bfeatsExDGEx[:e_ind-s_ind]

        imgen_scores[s_ind:e_ind] = np.squeeze(bgen_scores)
        imlogitsExDx[s_ind:e_ind] = np.squeeze(blogitsExDx)
        imfeatsExDx[s_ind:e_ind] = np.squeeze(bfeatsExDx)
        imlogitsExDGEx[s_ind:e_ind] = np.squeeze(blogitsExDGEx)
        imfeatsExDGEx[s_ind:e_ind] = np.squeeze(bfeatsExDGEx)

    return imgen_scores, imlogitsExDx, imfeatsExDx, imlogitsExDGEx, imfeatsExDGEx

#
def gen_distributions(aug_factor=10, held_out_set=False, held_out_ratio=0.1):

    global n

    c = [-1, .2, .1]

    # clean out the test set
    if held_out_set:
        if not os.path.isdir('heldout/'):
            os.mkdir('heldout/')
            print('created directory')
        else:
            hfiles = os.listdir('heldout/')
            for hf in hfiles:
                os.rename('heldout/' + hf, src + hf)
            print('remaining files:', len(os.listdir('heldout/')))

    bw_files = os.listdir(src)

    # create a held out test set on the fly
    if held_out_set and held_out_ratio > 0 and len(bw_files) > 0:
        print('creating heldout set...')
        if held_out_ratio > len(bw_files):
            held_out_ratio = len(bw_files)

        rand_idx = np.random.randint(0, len(bw_files), int(len(bw_files) * held_out_ratio))
        hfiles = [bw_files[i] for i in rand_idx]
        print('selected count:', len(hfiles))

        for hf in hfiles:
            if os.path.isfile(src + hf):
                os.rename(src + hf, 'heldout/' + hf)
        print('heldout files:', len(os.listdir('heldout/')))

        bw_files = os.listdir(src)

    h_files = os.listdir('heldout/')
    bw_files = list(set(bw_files) - set(h_files))

    print('total count:', len(bw_files))
    t_filecount = len(bw_files)

    fe = FeatureExtractor_2(config_dc)

    cur_count = 0

    for bw_file in bw_files:

        print('Processing file:', bw_file)
        bw = ski.imread(src + bw_file, mode=mode)
        root = os.path.splitext(bw_file)[0]

        print('Generating three channel map...')
        print('Extracting center features:', bw_file)
        bw, _ = fe.extract_image(bw, mode=mode)

        for a in range(aug_factor):
            bw_aug = bw
            if a > 0:
                bw_aug = tf5.__call__(
                    tf4.__call__(
                        tf3.__call__(
                            tf2.__call__(
                                tf1.__call__(bw)))))
                bw_aug = np.array(bw_aug)

            print('Extracting feature maps {} of {}'.format(a, aug_factor))
            imgen_scores, imlogitsExDx, imfeatsExDx, imlogitsExDGEx, imfeatsExDGEx = make_predictions(bw_aug, slc_dim)
            for k in range(n):
                slc_fn = '{}_augn_{}_slc_{}.npy'.format(root, a, k)
                np.save('gen_scores/{}'.format(slc_fn), imgen_scores)
                np.save('logitsExDx/{}'.format(slc_fn), imlogitsExDx)
                np.save('featsExDx/{}'.format(slc_fn), imfeatsExDx)
                np.save('logitsExDGEx/{}'.format(slc_fn), imlogitsExDGEx)
                np.save('featsExDGEx/{}'.format(slc_fn), imfeatsExDGEx)

        cur_count += 1
        print('Processed image {} of {}'.format(cur_count, t_filecount))
        print('')

    print('Map Generation is complete')

# IMAGE TRANSFORMATIONS
##########################################
# operations on the whole image
tf1 = transforms.ToPILImage()
tf2 = transforms.RandomHorizontalFlip()
tf3 = transforms.RandomVerticalFlip()
tf4 = transforms.RandomRotation(5)
tf5 = transforms.RandomAffine(degrees=0, translate=(0.0005, 0.0013))

# DOWNLOADS
##########################################
ds = [
    'bw_impellar_non_defects_2456x1842.zip',
    'defocused_bw_impellar_non_defects_2456x1842.zip',
    'robustness_bw_impellar_non_defects_2456x1842.zip'
    ]

src = 'bw_impellar_non_defects_2456x1842/'

download_files = False

if download_files:
    for f in ds:
        print('Downloading dataset...')
        os.system('cp {}{} {}'.format(datadir, f, ldir))
        os.system('unzip {}'.format(f))

        if f.split('.')[0] != src[:-1]:
            os.system('mv {}/* {}'.format(f.split('.')[0], src))

        print('Current file count: ',  len(os.listdir(src)))

    os.system('cp {}{} {}'.format(datadir, 'heldout.zip', ldir))
    os.system('unzip {}'.format('heldout.zip'))

else:
    print('Skipping image downloads')

# BUILD GRAPH
##########################################

if not os.path.isdir('gen_scores'):
    os.mkdir('gen_scores')
if not os.path.isdir('logitsExDx'):
    os.mkdir('logitsExDx')
if not os.path.isdir('featsExDx'):
    os.mkdir('featsExDx')
if not os.path.isdir('logitsExDGEx'):
    os.mkdir('logitsExDGEx')
if not os.path.isdir('featsExDGEx'):
    os.mkdir('featsExDGEx')

n = 0

with tf.Session() as sess:

    init = tf.global_variables_initializer()
    sess.run(init)
    saver = tf.train.import_meta_graph(model_dir + 'model.ckpt.meta')
    saver.restore(sess, model_dir + 'model.ckpt')
    graph = tf.get_default_graph()

    print('Building testing graph...')

    input_pl = graph.get_tensor_by_name("input:0")
    is_training_pl = graph.get_tensor_by_name('is_training_pl:0')

    with tf.variable_scope('encoder_model'):
        z_gen_ema = enc(input_pl, is_training=is_training_pl, reuse=True)




    with tf.variable_scope('generator_model'):
        reconstruct_ema = gen(z_gen_ema, is_training=is_training_pl, reuse=True)

    with tf.variable_scope('discriminator_model'):
        logitsExDx, featsExDx = dis(z_gen_ema, input_pl, is_training=is_training_pl, reuse=True)
        logitsExDGEx, featsExDGEx = dis(z_gen_ema, reconstruct_ema, is_training=is_training_pl, reuse=True)

    with tf.name_scope('Testing'):
        with tf.variable_scope('Reconstruction_loss'):
            delta = input_pl - reconstruct_ema
            delta_flat = tf.contrib.layers.flatten(delta)
            gen_score = tf.norm(delta_flat, ord=degree, axis=1, keep_dims=False, name='epsilon')

        with tf.variable_scope('Discriminator_loss'):
            fm = featsExDx - featsExDGEx
            fm = tf.contrib.layers.flatten(fm)
            dis_score = tf.norm(fm, ord=degree, axis=1, keep_dims=False, name='d_loss')
            dis_score = tf.squeeze(dis_score)

        # with tf.variable_scope('Score'):
        #     scores = (1 - weight) * gen_score + weight * dis_score

    gen_distributions(aug_factor=aug_factor)

exit(0)