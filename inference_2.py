
import numpy as np
import tensorflow as tf
import bigan.mnist_utilities_cond as network
import matplotlib.pyplot as plt
import scipy.misc as ski
from scipy.ndimage import gaussian_filter
from sklearn.preprocessing import OneHotEncoder

batch_size = network.batch_size
n_labels = 121
n_segs = 11
thresh = .01
input_sh = (batch_size, 28, 28, 1)
label_sh = (None, n_labels)
datadir = '/Users/evankingstad/bitbucket/data/ganomaly_v1/bigan_cond/train_logs/impellar/fm/0.1/1/42/'
imdir = '/Users/evankingstad/bitbucket/data/xpo-cv/images/bw_impellar_defects_308x308/'
fn = 'BW_Images_073018%2FImpeller_D25.00_side_2_1629.tiff'

def get_img_slices(arr, slice_dim):
    arr = np.squeeze(arr)
    shape_ = np.shape(arr)[:2]
    assert shape_[0] == shape_[1]
    arr_dim = shape_[0]
    slice_dim = min(shape_[0], shape_[1], slice_dim)
    # if arr_dim%slice_dim != 0:
    #     arr_dim = arr_dim+slice_dim-arr_dim%slice_dim
    #     arr = pad(arr, arr_dim, ret_n_cells=False, value=[255, 255, 255])
    n_segs = arr_dim//slice_dim
    if len(arr.shape) > 2:
        slices = np.empty((n_segs ** 2, slice_dim, slice_dim, arr.shape[-1])).astype(arr.dtype)
    else:
        slices = np.empty((n_segs**2, slice_dim, slice_dim)).astype(arr.dtype)
    for i in range(n_segs):
        y_init = i*slice_dim
        y_fin = (i+1)*slice_dim
        for j in range(n_segs):
            x_init = j * slice_dim
            x_fin = (j + 1) * slice_dim
            ind = coord_to_ind(i, j, n_segs)
            slices[ind] = arr[y_init:y_fin, x_init:x_fin]
    return slices

def coord_to_ind(i, j, n_segs):
    ind = int(i * n_segs + j)
    return ind

def make_soft_labels():
    encv = OneHotEncoder(n_values=n_labels, handle_unknown='ignore')
    lbls = np.arange(n_labels)
    lbls = encv.fit_transform(lbls.reshape(list(lbls.shape) + [-1])).toarray()
    lbls = lbls.reshape((n_labels, n_labels))
    p_lbls = np.zeros((n_labels, n_labels))
    for k, lbl in enumerate(lbls):
        p_lbl = lbl.reshape((n_segs, n_segs))
        p_lbl = gaussian_filter(p_lbl, sigma=.5)
        p_lbl[p_lbl < thresh] = 0
        p_lbls[k] = (p_lbl / p_lbl.sum()).ravel()
    return p_lbls

X = get_img_slices(ski.imread(imdir + fn, mode='L'), 28)

saver = tf.train.import_meta_graph(datadir + 'model.ckpt.meta')
with tf.Session() as sess:
    saver.restore(sess, datadir + 'model.ckpt')

    input_pl = tf.placeholder(tf.float32, shape=input_sh, name="input")
    label_pl = tf.placeholder(tf.float32, shape=label_sh, name="label")
    is_training_pl = tf.placeholder(tf.bool, [], name='is_training_pl')

    gen = network.decoder
    enc = network.encoder

    with tf.variable_scope('encoder_model'):
        z_gen_ema = enc(input_pl, label_pl, is_training=is_training_pl)

    with tf.variable_scope('generator_model'):
        reconstruct_ema = gen(z_gen_ema, label_pl, is_training=is_training_pl)

    sess.run(tf.global_variables_initializer())

    batchx = np.expand_dims(X / 127.5 - 1, axis=-1).astype('float32')
    batchy = make_soft_labels().astype('float32')
    out1 = sess.run(reconstruct_ema, feed_dict={input_pl: batchx[:100], label_pl: batchy[:100], is_training_pl: False})

ind = np.random.randint(0, len(out1))
sample = np.squeeze(out1[ind])
sample += np.abs(np.min(sample))
sample /= np.max(sample)
sample *= 255
sample = sample.astype('uint8')
conc = np.concatenate((sample, X[ind]))
plt.imshow(conc, cmap = 'gray')