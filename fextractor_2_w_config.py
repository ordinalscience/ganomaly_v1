# Initialization
# --------
# if using Array
# im = FeatureExtractor(*INSERT ARRAY HERE*,isArray=True)

# if using Image path
# im = FeatureExtractor(*INSERT PATH HERE*)

# can also specify output size for either(array,image path)
# im = FeatureExtractor(*INSERT PATH HERE*, fullSize = INT)

# Output
# --------
# saves centered image to savelocation
# im.center_and_save_image(SaveLocation)

# returns an array of centered image
# im.center_and_return_array()


import numpy as np
import scipy.misc as ski
import cv2
from cv2 import GaussianBlur
from skimage import filters
from scipy import ndimage
from scipy.ndimage.morphology import binary_fill_holes

class FeatureExtractor_2:
    img_array = None

    height = None
    width = None

    resize_img = None

    def __init__(self, config_dc):
        self.factor = config_dc['factor']
        self.fullSize = config_dc['fullSize']
        self.h = config_dc['h']
        self.black_thresh = config_dc['black_thresh']

    #
    def _recolor(self, bw):
        clr = np.zeros((bw.shape[0], bw.shape[1], 3))
        bw = bw.astype(float)

        c = [-1, .2, .1]
        for i in range(3):
            if i == 0:
                clr[:, :, i] = bw * ((1 - (bw / 255.0)) * .8 + 1)
            else:
                clr[bw < 230, i] = bw[bw < 230] * c[i]
            clr[bw > 230, i] = bw[bw > 230]

        return clr

    #
    def _noise_mask(self, img):
        height = img.shape[0] // self.factor
        width = img.shape[1] // self.factor

        r_image = ski.imresize(img, (height, width))

        corners = [(-1, -1), (1, 1), (1, -1), (-1, 1)]
        image2 = np.zeros((height, width))
        x_range = np.arange(1, width - 1)

        for y in range(1, height - 1):
            for x in x_range:
                pixel1 = r_image[y, x]
                red = int(pixel1[0])
                green = int(pixel1[1])
                blue = int(pixel1[2])
                dr = 0
                dg = 0
                db = 0
                for corner in corners:
                    pixel2 = r_image[y + corner[0], x + corner[1]]
                    dr = dr + abs(red - int(pixel2[0]))
                    dg = dg + abs(green - int(pixel2[1]))
                    db = db + abs(blue - int(pixel2[2]))
                value = np.minimum(255, (3 * dr + 5 * dg + 2 * db) - 255 // 20)
                image2[y, x] = value

        return image2

    #
    def _clarify(self, grid):
        height = grid.shape[0]
        width = grid.shape[1]
        grid2 = np.zeros((height, width))
        corners = [(1, 0), (0, 1), (-1, 0), (0, -1)]
        x_range = np.arange(1, width - 1)
        for y in range(1, height - 1):
            for x in x_range:
                value = grid[y, x]
                if value == 255:
                    for corner in corners:
                        if grid[y + corner[0], x + corner[1]] < 255:
                            value = 0
                            break
                else:
                    value = 0
                grid2[y, x] = value
        return grid2

    #
    def get_internal_mask_old (self, o_img, K):
        o_shape = o_img.shape
        img = o_img.copy()

        for step in range(K - 1):
            blur = GaussianBlur(img, ksize=(3, 3), sigmaX=1, sigmaY=1)
            img = ski.imresize(blur, (int(img.shape[0] / 2), int(img.shape[1] / 2)))

        thresh = filters.threshold_otsu(img)
        internal_mask = (img > thresh).astype('uint8')
        internal_mask = (internal_mask + 1) % 2
        internal_mask = binary_fill_holes(internal_mask).astype(int)
        internal_mask = ski.imresize(internal_mask, (o_shape[0], o_shape[1]))
        internal_mask[internal_mask <= .5] = 0
        internal_mask[internal_mask > .5] = 1

        return internal_mask

    #
    def get_internal_mask(self, img, h, black_thresh):
        img = cv2.fastNlMeansDenoising(img, h=h)
        img.setflags(write=True)
        img[img < black_thresh] = 255
        thresh = filters.threshold_otsu(img)
        otsu_img = binary_fill_holes(((img > thresh) + 1) % 2).astype(int)
        return otsu_img

    #
    def _find_bounds(self, grid, minPixels=3, count=7):
        height = grid.shape[0]
        width = grid.shape[1]
        xTotal = 0
        yTotal = 0
        total = 0
        x_range = np.arange(0, width)
        for y in range(0, height):
            for x in x_range:
                value = grid[y, x]
                xTotal = xTotal + x * value
                yTotal = yTotal + y * value
                total = total + value
        centerX = int(xTotal / total)
        centerY = int(yTotal / total)

        spanEnd = np.minimum(centerX, width - centerX)
        strikes = 0
        for span in range(1, spanEnd):
            total = 0
            for y in range(0, height):
                if grid[y, centerX + span] > 0:
                    total = total + 1
                    if total == minPixels:
                        break
            if total < minPixels:
                strikes = strikes + 1
            elif strikes > 0:
                strikes = strikes - 1
            if strikes == count:
                break
        spanX = span

        spanEnd = np.minimum(centerY, height - centerY)
        strikes = 0
        x_range = np.arange(0, width)
        for span in range(1, spanEnd):
            total = 0
            for x in x_range:
                if grid[centerY + span, x] > 0:
                    total = total + 1
                    if total == minPixels:
                        break
            if total < minPixels:
                strikes = strikes + 1
            elif strikes > 0:
                strikes = strikes - 1
            if strikes == count:
                break
        spanY = span

        return {"centerX": centerX, "centerY": centerY, "spanX": spanX, "spanY": spanY}

    #
    def get_bounds(self, img):
        n_mask = self._noise_mask(img)
        n_c_mask = self._clarify(n_mask)
        bounds = self._find_bounds(n_c_mask)

        girth = np.maximum(bounds["spanX"], bounds["spanY"]) * self.factor + 20
        cx = bounds["centerX"] * self.factor
        cy = bounds["centerY"] * self.factor

        min_y_diff = min(abs(cy - max(0, cy - girth)), abs(cy - min(img.shape[0], cy + girth)))
        min_x_diff = min(abs(cx - max(0, cx - girth)), abs(cx - min(img.shape[1], cx + girth)))
        min_diff = min(min_y_diff, min_x_diff)

        return cy, cx, min_diff

    #
    def center_and_return_array(self, img, bounds, int_mask):
        cy, cx, min_diff = bounds

        # new_img = Image.fromarray(img[cy - min_diff:cy + min_diff, cx - min_diff:cx + min_diff])
        # resize_img = new_img.resize((self.fullSize, self.fullSize), Image.ANTIALIAS)

        new_img = img[cy - min_diff:cy + min_diff, cx - min_diff:cx + min_diff]
        new_img = ski.imresize(new_img, (self.fullSize, self.fullSize))

        int_mask = int_mask[cy - min_diff:cy + min_diff, cx - min_diff:cx + min_diff]
        int_mask = ski.imresize(int_mask, (self.fullSize, self.fullSize))
        int_mask = (int_mask > .5).astype('uint8')

        return new_img, int_mask

    #
    def extract_image(self, img, clean_flare=True, mode = 'RGB'):
        bounds = None
        internal_mask = None

        bw = None
        if clean_flare:
            bw = img.copy()
            bw_c = bw.copy()

        if len(img.shape) < 3:
            img = self._recolor(img)

        img = img.astype(np.uint8)

        if clean_flare:
            # cleaning flare
            internal_mask = self.get_internal_mask(bw, self.h, self.black_thresh)
            internal_mask, num_features = ndimage.label(internal_mask)
            un = np.unique(internal_mask)[1:]
            counts, buckets = np.histogram(internal_mask, bins=np.append(un, un[-1] + 1))
            buckets = buckets[:-1]
            internal_mask = (internal_mask == buckets[np.argmax(counts)]).astype('uint8')
            bw_c[internal_mask == 0] = 255
            img_masked = np.repeat(bw_c.reshape(bw_c.shape[0], bw_c.shape[1], 1), 3, axis=2)
            bounds = self.get_bounds(img_masked)
        else:
            bounds = self.get_bounds(img)

        if mode == 'RGB':
            pass
        elif mode == 'L':
            img = bw

        return self.center_and_return_array(img, bounds, internal_mask)
