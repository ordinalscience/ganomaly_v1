#
import numpy as np
import os
import scipy.misc as ski

#
ldir = '/home/dev/ganomaly_v1/abigan/'
datadir = '/home/dev/data/xpo-cv/'


sets = ['train', 'val', 'test']
cls = [0,1,2,3]

download_files = False
if download_files:
    #
    ds = [
        'DS2_3289_BW_original_June.zip',
        'DS3_3289_BW_robustness_Sept.zip',
        'DS4_3289_BW_defocused_Sept.zip'
    ]

    #
    for f in ds:
        os.system('cp {}{} {}'.format(datadir, f, ldir))
        os.system('unzip {}'.format(f))
        if f[:-4] != 'DS2_3289_BW_original_June/':
            for s in sets:
                for c in cls:
                    print('{}/{}/{}/'.format(f[:-4], s, c))
                    for i in range(121):
                        os.system(
                            'mv {}/{}/{}/*_{}.tiff {}/{}/{}/'.format(f[:-4], s, c, i, 'DS2_3289_BW_original_June', s,
                                                                     c))

#
    for s in sets:
        for c in cls:
            if s != 'train' or c > 0:
                os.system(
                    'mv {}/{}/{}/* {}/{}/{}/'.format('DS2_3289_BW_original_June', s, c, 'DS2_3289_BW_original_June',
                                                     'train', c))


#
def _recolor(bw):
    clr = np.zeros((bw.shape[0], bw.shape[1], 3))
    bw = bw.astype(float)

    c = [-1, .2, .1]
    for i in range(3):
        if i == 0:
            clr[:, :, i] = bw * ((1 - (bw / 255.0)) * .8 + 1)
        else:
            clr[bw < 230, i] = bw[bw < 230] * c[i]
        clr[bw > 230, i] = bw[bw > 230]

    return clr

#
if not os.path.isdir('DS2_3289_BW_original_June_64x64x3'):
    os.mkdir('DS2_3289_BW_original_June_64x64x3')

#
if not os.path.isdir('DS2_3289_BW_original_June_64x64x3/train'):
    os.mkdir('DS2_3289_BW_original_June_64x64x3/train')

#
for c in cls:
    c_src = c
    c_dst = int(c > 0)
    src = 'DS2_3289_BW_original_June/train/{}/'.format(c_src)
    dst = 'DS2_3289_BW_original_June_64x64x3/train/{}/'.format(c_dst)
    if not os.path.isdir(dst):
        os.mkdir(dst)
    trainx = np.array(os.listdir(src))
    np.random.shuffle(trainx)
    print(trainx.shape)
    N = len(trainx)
    for i, rfn in enumerate(trainx):
        if rfn.endswith('.tif'):
            wfn = rfn[:-4]
        elif rfn.endswith('.tiff'):
            wfn = rfn[:-5]
        else:
            continue
        wfn += '_{}.tiff'.format(c)
        bw = ski.imread(src + rfn, mode='L')
        clr = _recolor(bw)
        clr = ski.imresize(clr, (64, 64, 3))
        ski.imsave(dst + wfn, clr)
        if ((i + 1) % 1000) == 0:
            print('Through {} of {}'.format(i + 1, N))

