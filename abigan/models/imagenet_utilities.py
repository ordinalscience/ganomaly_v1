
# abigan --> [a] [big] [bi[g]an]

import numpy as npdata
import tensorflow as tf
from abigan.models.ops import *
from functools import reduce
from collections import namedtuple
from abigan.utils import sn

w_init = lambda:tf.truncated_normal_initializer(mean=0.0, stddev=0.001)

slope = 0.01
learning_rate = 0.0001
latent_dim=256
batch_size = 64
beta1 = 0.5
beta2 = 0.999
image_shape=(64, 64, 3)

#
def encoder(x_inp, is_training=False, getter=None, reuse=False, do_spectral_norm=True):

    """ Encoder architecture in tensorflow

        Maps the data into the latent space

        Args:
            x_inp (tensor): input data for the encoder.
            reuse (bool): sharing variables or not

        Returns:
            (tensor): last activation layer of the encoder

    """

    layers = sn if do_spectral_norm else tf.layers

    with tf.variable_scope('encoder', reuse=reuse, custom_getter=getter) as scope:
        h = x_inp

        Block = namedtuple('Block', ['i', 'filter_num', 'kernel_size', 'stride'])
        blocks = [
            Block(0, 64, 4, 2),
            Block(1, 64, 4, 1),
            Block(2, 128, 4, 2),
            Block(3, 128, 4, 1),
            Block(4, 256, 4, 2),
            Block(5, 256, 4, 1),
            Block(6, 2048, 1, 1),
            Block(7, 2048, 1, 1),
        ]

        for b in blocks:
            with tf.variable_scope('block{}'.format(b.i)):
                h = layers.conv2d(h,
                                  filters=b.filter_num,
                                  kernel_size=b.kernel_size,
                                  strides=b.stride,
                                  padding='VALID',
                                  use_bias=False,
                                  kernel_initializer=w_init(),
                                  name='conv')
                h = batch_norm(h, training=is_training)
                h = lrelu(h, slope=slope)

        with tf.variable_scope('block8'):
            with tf.variable_scope('mu'):
                h_mu = layers.conv2d(h,
                                     filters=latent_dim,
                                     kernel_size=1,
                                     strides=1,
                                     padding='VALID',
                                     use_bias=False,
                                     kernel_initializer=w_init(),
                                     name='conv')
                h_mu = add_nontied_bias(h_mu)
            with tf.variable_scope('sigma'):
                h_sigma = layers.conv2d(h,
                                        filters=latent_dim,
                                        kernel_size=1,
                                        strides=1,
                                        padding='VALID',
                                        use_bias=False,
                                        kernel_initializer=w_init(),
                                        name='conv')
                h_sigma = add_nontied_bias(h_sigma)
                h_sigma = tf.exp(h_sigma)

            rng = tf.random_normal(shape=tf.shape(h_mu))
            output = (rng * h_sigma) + h_mu

    return output

#
def decoder(z_inp, initializer = None, is_training=False, getter=None, reuse=False):
    """ Decoder architecture in tensorflow

        Generates data from the latent space

        Args:
            z_inp (tensor): variable in the latent space
            reuse (bool): sharing variables or not

        Returns:
            (tensor): last activation layer of the generator

    """
    with tf.variable_scope('generator', reuse=reuse, custom_getter=getter) as scope:

        h = z_inp

        with tf.variable_scope('block0'):
            h = tf.layers.conv2d(h,
                                 filters=2048,
                                 kernel_size=(1, 1),
                                 strides=(1, 1),
                                 padding='VALID',
                                 use_bias=False,
                                 kernel_initializer=w_init(),
                                 name='conv')
            h = batch_norm(h, training=is_training)
            h = lrelu(h, slope=slope)

        with tf.variable_scope('block1'):
            h = tf.layers.conv2d(h,
                                 filters=256,
                                 kernel_size=(1, 1),
                                 strides=(1, 1),
                                 padding='VALID',
                                 use_bias=False,
                                 kernel_initializer=w_init(),
                                 name='conv')
            h = batch_norm(h, training=is_training)
            h = lrelu(h, slope=slope)

        Block = namedtuple('Block', ['i', 'filter_num', 'kernel_size', 'stride'])
        blocks = [
            Block(2, 256, 4, 1),
            Block(3, 128, 4, 2),
            Block(4, 128, 4, 1),
            Block(5, 64, 4, 2),
            Block(6, 64, 4, 1),
            Block(7, 64, 4, 2),
        ]

        for i, b in enumerate(blocks):
            with tf.variable_scope('block{}'.format(b.i)):
                h = tf.layers.conv2d_transpose(h,
                                               filters=b.filter_num,
                                               kernel_size=b.kernel_size,
                                               strides=b.stride,
                                               padding='VALID',
                                               use_bias=False,
                                               kernel_initializer=w_init(),
                                               name='tconv')
                h = batch_norm(h, training=is_training)
                h = lrelu(h, slope=slope)

        with tf.variable_scope('block8'):
            h = tf.layers.conv2d(h,
                                 filters=3,
                                 kernel_size=(1, 1),
                                 strides=(1, 1),
                                 padding='VALID',
                                 use_bias=False,
                                 kernel_initializer=w_init(),
                                 name='conv')
            h = add_nontied_bias(h, initializer=initializer)
            h = tf.nn.sigmoid(h)

        output = h

    return output

def discriminator(z_inp, x_inp, is_training=False, getter=None, reuse=False, do_spectral_norm=True):

    layers = sn if do_spectral_norm else tf.layers

    with tf.variable_scope('discriminator', reuse=reuse, custom_getter=getter):

        h_x = x_inp
        h_z = z_inp

        with tf.variable_scope('x'):

            Block = namedtuple('Block', ['i', 'filter_num', 'kernel_size', 'stride', 'is_bn'])
            blocks = [
                Block(0, 64, 4, 2, False),
                Block(1, 64, 4, 1, True),
                Block(2, 128, 4, 2, True),
                Block(3, 128, 4, 1, True),
                Block(4, 256, 4, 2, True),
                Block(5, 256, 4, 1, True),
            ]

            for b in blocks:
                with tf.variable_scope('block{}'.format(b.i)):
                    h_x = tf.layers.dropout(h_x, rate=0.2, training=is_training)
                    h_x = layers.conv2d(h_x,
                                        filters=b.filter_num,
                                        kernel_size=b.kernel_size,
                                        strides=b.stride,
                                        padding='VALID',
                                        use_bias=not b.is_bn,
                                        kernel_initializer=w_init(),
                                        name='conv')
                    if b.is_bn:
                        h_x = batch_norm(h_x, training=is_training)
                    h_x = lrelu(h_x, slope=slope)

        with tf.variable_scope('z'):

            with tf.variable_scope('block0'):
                h_z = tf.layers.dropout(h_z, rate=0.2, training=is_training)
                h_z = layers.conv2d(h_z,
                                    filters=2048,
                                    kernel_size=1,
                                    strides=1,
                                    padding='VALID',
                                    use_bias=True,
                                    kernel_initializer=w_init(),
                                    name='conv')
                h_z = lrelu(h_z, slope=slope)

            with tf.variable_scope('block1'):
                h_z = tf.layers.dropout(h_z, rate=0.2, training=is_training)
                h_z = layers.conv2d(h_z,
                                    filters=2048,
                                    kernel_size=1,
                                    strides=1,
                                    padding='VALID',
                                    use_bias=True,
                                    kernel_initializer=w_init(),
                                    name='conv')
                h_z = lrelu(h_z, slope=slope)

        with tf.variable_scope('xz'):

            h_xz = tf.concat([h_x, h_z], axis=h_x.get_shape().ndims - 1)

            with tf.variable_scope('block0'):
                h_xz = tf.layers.dropout(h_xz, rate=0.2, training=is_training)
                h_xz = layers.conv2d(h_xz,
                                     filters=4096,
                                     kernel_size=1,
                                     strides=1,
                                     padding='VALID',
                                     use_bias=True,
                                     kernel_initializer=w_init(),
                                     name='conv')
                h_xz = lrelu(h_xz, slope=slope)

            with tf.variable_scope('block1'):
                h_xz = tf.layers.dropout(h_xz, rate=0.2, training=is_training)
                h_xz = layers.conv2d(h_xz,
                                     filters=4096,
                                     kernel_size=1,
                                     strides=1,
                                     padding='VALID',
                                     use_bias=True,
                                     kernel_initializer=w_init(),
                                     name='conv')
                h_xz = lrelu(h_xz, slope=slope)

            with tf.variable_scope('block2'):
                intermediate_layer = tf.layers.dropout(h_xz, rate=0.2, training=is_training)
                # TODO: Right here is the analogous 'intermediate layer' output... dropout following N-dim feat vec.

                logits = layers.conv2d(intermediate_layer,
                                       filters=1,
                                       kernel_size=1,
                                       strides=1,
                                       padding='VALID',
                                       use_bias=True,
                                       kernel_initializer=w_init(),
                                       name='conv')

        return logits, intermediate_layer
